//
//  playerVC.swift
//  MusicPlayerApp
//
//  Created by amr on 2/7/18.
//  Copyright © 2018 amr. All rights reserved.
//
import AVFoundation
import UIKit

class playerVC: UIViewController {
   
    var songID = -1
    
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var timerSlider: UISlider!
    @IBOutlet weak var artistNameLbl: UILabel!
    @IBOutlet weak var songImage: UIImageView!
    @IBOutlet weak var songNameLbl: UILabel!
    var player =  AVAudioPlayer()
    var playPauseFlag = 0
    var flag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       updateValues()
        initPlayer()
    }
    
    @IBOutlet weak var playPausebtn: UIButton!
    @IBOutlet weak var nextSongBtn: UIButton!
    @IBAction func playPauseClicked(_ sender: Any) {
     
        if flag == 0{
            player.play()
            playPausebtn.setImage(UIImage(named: "pause"), for: .normal)
            flag = 1
            
        }
        else {
            player.pause()
            playPausebtn.setImage(UIImage(named: "play"), for: .normal)
            flag = 0
        }
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
  
    @IBAction func volumeChanged(_ sender: UISlider) {
         player.volume = sender.value
    }
    @IBAction func previousSongClicked(_ sender: Any) {
        songID = previousSongIdCheck(soongID: songID)
        updateValues()
        initPlayer()
    }
    @IBAction func nextSongClicked(_ sender: Any) {
        songID = nextSongIdCheck(soongID: songID)
        updateValues()
        initPlayer()
    }
    @IBOutlet weak var previousSongBtn: UIButton!
    func updateValues(){
        let soong = DataService.instance.getSongs()[songID]
        songNameLbl.text = soong.title
        artistNameLbl.text = soong.artist
        songImage.image = UIImage(named: soong.artwork)
        
    }
   
    
    func initPlayer(){
        let audioPath = Bundle.main.path(forResource: String(songID), ofType: "mp3")
        
        do {
            //Load that audio file into the player
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
            
            
        } catch {
            let alert = UIAlertController(title: "oops", message: "This song doesn't exist \(songID)", preferredStyle: .alert)
            let okayBtn = UIAlertAction(title: "okay", style: .default, handler: {(action) in alert.dismiss(animated: true, completion: nil)})
            alert.addAction(okayBtn)
            self.present(alert, animated: true, completion: nil)
        }
          playPausebtn.setImage(UIImage(named: "play"), for: .normal)
        timerSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        //Set the time slider max value to match the duration of the song
        timerSlider.maximumValue = Float(player.duration)
        
        //Set the initial value of the volume slider to match the player volume at the beginning
        volumeSlider.value = player.volume
        
        
        volumeSlider.value = player.volume
      player.prepareToPlay()
       
        
    }
    
    @objc func updateTimer(){
        timerSlider.setValue(Float(player.currentTime), animated: true)
    }
    
    func nextSongIdCheck(soongID : Int) -> Int{
        let songCount = DataService.instance.getSongs().count - 1
        
     if soongID == songCount
     {
        return 0
        
        }
     else {
        return soongID + 1
        
        }
    }
    
    func previousSongIdCheck(soongID : Int) -> Int{
        let songCount = DataService.instance.getSongs().count - 1
        
        if soongID == 0
        {
            return songCount
            
        }
        else {
            return soongID - 1
            
        }
    }
    
  
    }

 



