//
//  ViewController.swift
//  MusicPlayerApp
//
//  Created by amr on 2/7/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class musicVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var songsTV: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        songsTV.delegate = self
        songsTV.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getSongs().count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell") as? SongCell{
            let song = DataService.instance.getSongs()[indexPath.row]
            cell.updateValues(song: song)
            return cell
        }
        else {
            return SongCell()
    }


}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //make sure this is the segue you want
        if segue.identifier == "listToPlayer" {
            
            //access the view controller you are moving to
            let nextVC = segue.destination as! playerVC
            
            //update its value with the current value
            
            
            //get the selected row
            let selectedSong = songsTV.indexPathForSelectedRow?.row
            
            //update the second view controller' songID to the new value = selected song
            nextVC.songID = selectedSong!
            
        }
        
    }
}
