//
//  SongCell.swift
//  MusicPlayerApp
//
//  Created by amr on 2/7/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class SongCell: UITableViewCell {

    @IBOutlet weak var songArtistLbl: UILabel!
    @IBOutlet weak var songNameLbl: UILabel!
    @IBOutlet weak var songImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateValues(song : Song){
        songNameLbl.text = song.title
        songArtistLbl.text = song.artist
        songImage.image = UIImage(named: song.artwork)
        
    }
}
