//
//  DataService.swift
//  MusicPlayerApp
//
//  Created by amr on 2/7/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation
class DataService {
     static let instance = DataService()
    private let songs = [
    Song(title: "Weak", artist:"Ajr", artwork:"0"),Song(title: "Billi Gean", artist:"Michael Jackson", artwork:"1"), Song(title: "24 K magic", artist:"Bruno Mars", artwork:"2"), Song(title: "Panda", artist:"Desiigner", artwork:"3"), Song(title: "Hello", artist:"Adele", artwork:"4"), Song(title: "Love on the brain", artist:"Rihanna", artwork:"5"), Song(title: "Malibu", artist:"Miley Cyrus", artwork:"6"), Song(title: "Scar Tissue", artist:"red hot chili peppers", artwork:"7")
    ]
    
    func getSongs() ->[Song]{
        return songs
    }
    
}
