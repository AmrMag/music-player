//
//  Song.swift
//  MusicPlayerApp
//
//  Created by amr on 2/7/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation

struct Song{
    private(set) public var title : String
    private(set) public var artist : String
    private(set) public var artwork : String
    
    init(title : String, artist : String, artwork : String){
        self.title = title
        self.artist = artist
        self.artwork = artwork
    }
}
